import { Component, Inject, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TransferState, makeStateKey } from '@angular/platform-browser';

const PAGE_DATA_KEY = makeStateKey('showforum');

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  public PageData: object;
  constructor(http: HttpClient,
    state: TransferState,
    @Optional() @Inject('BASE_URL') baseUrl: string,
    @Optional() @Inject('BootFuncParams') base: object) {

    if (!this.PageData) {
      state.set(makeStateKey('BootFuncParams'), base);
      http.get<object>('http://localhost:12019/api/page/showforum').subscribe(success => {
        this.PageData = success;
        state.set(PAGE_DATA_KEY, success as any);
      }, error => {
        console.log(error);
      });
    }

  }

}
